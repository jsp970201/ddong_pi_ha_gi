using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UI;
using UnityEngine;


public class PlayerMove : MonoBehaviour
{
    private Rigidbody2D rigidbody;

    private float speed = 5.0f;

    private float horizontal;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        playerMove();
    }

    private void playerMove()
    {
        rigidbody.velocity = new Vector2 (horizontal * speed, rigidbody.velocity.y);
    }
}
