using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private Slider hpbar;

    private float MaxHp = 100.0f;

    private float CurHp = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        hpbar.value = (float)CurHp / (float)MaxHp;
    }

    // Update is called once per frame
    void Update()
    {
            if (CurHp > 0)
            {
                CurHp -= 10.0f;
            }
            else
            {
                CurHp = 0.0f;
                Destroy(player);
            }

        HandleHp();
    }

    private void HandleHp()
    {
        hpbar.value = (float)CurHp / (float)MaxHp;
    }
}
